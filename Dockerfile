#
# Dockerfile for tinyproxy
#

FROM alpine
MAINTAINER katocz <katocz@net1.cz>

RUN apk --no-cache add ca-certificates
RUN set -xe \
    && apk --no-cache add tinyproxy \
    && sed -i -e '/^Allow /s/^/#/' \
              -e '/^ConnectPort /s/^/#/' \
              -e '/^#DisableViaHeader /s/^#//' \
              /etc/tinyproxy/tinyproxy.conf

WORKDIR /usr/local/bin

CMD ["tinyproxy"]
